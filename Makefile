VERSION := $(shell ./version)

all: run

run:
	cd cordova && cordova run android

setup:
	cd cordova && cordova platform add android

dist:
	./disable-stats
	./build-release --packageType=apk

dist-play-store:
	./enable-stats
	./build-release

upload: dist
	scp \
		cordova/platforms/android/app/build/outputs/apk/release/app-release.apk \
		box-stacker.artificialworlds.net:box-stacker.artificialworlds.net/builds/android/box-stacker-${VERSION}.apk

