# Box Stacker Mobile

This project packages [Box Stacker](https://gitlab.com/box-stacker/box-stacker)
for Android using [Apache Cordova](https://cordova.apache.org).

In future it might also be able to build for iOS (contributions welcome).

## Prerequisites

* Install [nodejs and npm](https://nodejs.org)

* Install all the [system requirements for Apache Cordova on Android](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#requirements-and-support).
  At the time of writing this is Java 11, Gradle, Android Studio, and some
  environment variables.

* Install Apache Cordova:

```bash
cd cordova
npm install -g cordova
```

* Add the Android platform

```bash
cd cordova
cordova platform add android

```
## Development

To launch on your connected Android device/emulator:

```bash
cd cordova
cordova run android
```

Or to build the apk manually and install it manually on your connected device:

```bash
cd cordova
cordova build android
adb install platforms/android/app/build/outputs/apk/debug/app-debug.apk
```

To update the box-stacker version:

* Modify the version number in config.xml to start with the version of
  box-stacker you want

* Make sure you update the android-versionCode property in config.xml too.

* Run the updater:

```bash
./update-box-stacker
```

## Releases

To build the aab file for the Android Play Store:

```bash
make dist-play-store
```

To build a release apk to upload for manual installs, and for use as a reference
by the F-Droid build:

```bash
make dist
```

To upload the file that F-Droid uses as a reference (obviously only if you have
the relevant permission):

```bash
make upload
```

## F-Droid

We use reproducible builds on F-Droid, meaning that we manually upload a signed
apk, and F-Droid verifies that it matches our source code, and then actually
provides the apk signed by us.

The public page for Box Stacker on F-Droid is
[f-droid.org/packages/net.artificialworlds.boxstacker](https://f-droid.org/packages/net.artificialworlds.boxstacker)

The current Box Stacker metadata file is
[net.artificialworlds.boxstacker.yml](https://gitlab.com/fdroid/fdroiddata/-/blob/master/metadata/net.artificialworlds.boxstacker.yml)

The merge request that added Box Stacker to F-Droid is
[#13226](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/13226)

## License

Copyright 2022-2023 Andy Balaam, Codesmith00, RayDuck and the Box Stacker
contributors.

Released under the AGPLv3 license or later. See [LICENSE](LICENSE) for info.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)

In addition, this project is child-friendly, so please be extra-careful to be
polite, understanding and respectful at all times.
