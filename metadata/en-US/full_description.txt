Box Stacker is a physics-based construction puzzle game.

Build machines and structures that help balls, cars and other objects get to where they need to be.

You can build from simple blocks but create quite clever machines.

There are lots of easy levels and lots of hard ones, and even a few extra-hard Extremes if you like a challenge!

You can also make you own levels and share them with other players in our forum!
